package com.reedex.convenience;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

public class ErrorDialogFragment extends DialogFragment {
    private static final String ARG_TAG = "tag";
    private static final String ARG_TITLE = "title";
    private static final String ARG_MESSAGE = "message";
    private static final String ARG_ONE_BUTTON = "one_button";
    private DialogListener listener;

    public static ErrorDialogFragment newInstance(int tag, String title, String message) {
        ErrorDialogFragment dialog = new ErrorDialogFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_TAG, tag);
        args.putString(ARG_TITLE, title);
        args.putString(ARG_MESSAGE, message);
        dialog.setArguments(args);
        return dialog;
    }

    public static ErrorDialogFragment newInstance(String title, String message) {
        ErrorDialogFragment dialog = new ErrorDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        args.putString(ARG_MESSAGE, message);
        dialog.setArguments(args);
        return dialog;
    }

    public static ErrorDialogFragment newInstance(String title, String message, boolean isOneButton) {
        ErrorDialogFragment dialog = new ErrorDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        args.putString(ARG_MESSAGE, message);
        args.putBoolean(ARG_ONE_BUTTON, isOneButton);
        dialog.setArguments(args);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        final int tag = args.getInt(ARG_TAG);
        String title = args.getString(ARG_TITLE);
        String message = args.getString(ARG_MESSAGE);
        boolean isOneButton = args.getBoolean(ARG_ONE_BUTTON);

        if (getActivity() instanceof DialogListener) {
            listener = (DialogListener) getActivity();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();

                        if (listener != null) {
                            listener.errorDialogPositive(tag);
                        }
                    }
                });

        if (!isOneButton) {
            builder.setNegativeButton("キャンセル", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dismiss();
                }
            });
        }
        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    public interface DialogListener {
        void errorDialogPositive(int tag);
    }
}
