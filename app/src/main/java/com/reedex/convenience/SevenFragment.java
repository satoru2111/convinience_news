package com.reedex.convenience;

import android.os.Bundle;

public class SevenFragment extends BaseFragment {

    public SevenFragment() {
        // Required empty public constructor
    }

    public static SevenFragment newInstance() {
        return new SevenFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void updateNews() {
        mSwipeRefresh.setRefreshing(true);
        resetFlag();
    }
}
