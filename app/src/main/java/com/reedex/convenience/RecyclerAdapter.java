package com.reedex.convenience;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private List<Item> mData;
    private Context mContext;
    private OnRecyclerListener mListener;
    private SimpleDateFormat mFormatBase = new SimpleDateFormat("yyyy.M.dd HH:mm");

    public RecyclerAdapter(Context context, List<Item> data, OnRecyclerListener listener) {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mData = data;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        // 表示するレイアウトを設定
        return new ViewHolder(mInflater.inflate(R.layout.list_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        // データ表示
        if (mData != null && mData.size() > i && mData.get(i) != null) {
            Item item = mData.get(i);
            viewHolder.imageThumbnail.setVisibility(View.VISIBLE);
            viewHolder.textTitle.setText(item.title);
            viewHolder.textSite.setText(item.site);

            if (item.isRead) {
                viewHolder.itemView.setBackgroundResource(R.drawable.button_gray);
                viewHolder.textTitle.setTextColor(mContext.getResources().getColor(R.color.read));
            } else {
                viewHolder.itemView.setBackgroundResource(R.drawable.button_white);
                viewHolder.textTitle.setTextColor(mContext.getResources().getColor(R.color.text));
            }
            viewHolder.textDate.setText(changeBefore(item.date));
            if (!item.imageUrl.isEmpty()) {
                Picasso.with(mContext).load(item.imageUrl).fit().centerCrop().into(viewHolder.imageThumbnail, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {
                        viewHolder.imageThumbnail.setVisibility(View.GONE);
                    }
                });
            } else {
                viewHolder.imageThumbnail.setVisibility(View.GONE);
            }
        }

        // クリック処理
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onRecyclerClicked(v, viewHolder.getAdapterPosition());
            }
        });

    }

    private String changeBefore(String stringDate) {
        Date date = null;
        try {
            date = mFormatBase.parse(Calendar.getInstance().get(Calendar.YEAR) + "." + stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = Calendar.getInstance().getTimeInMillis() - date.getTime();
        if (diff > 0 && diff < 1000 * 60 * 60 * 24) {
            if (diff < 1000 * 60 * 60) {
                return String.valueOf(diff / (1000 * 60)) + "分前";
            } else {
                return String.valueOf(diff / (1000 * 60 * 60)) + "時間前";
            }
        } else {
            return stringDate.replace(" 00:00", "");
        }

    }

    @Override
    public int getItemCount() {
        if (mData != null) {
            return mData.size();
        } else {
            return 0;
        }
    }

    // ViewHolder(固有ならインナークラスでOK)
    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageThumbnail;
        TextView textTitle;
        TextView textDate;
        TextView textSite;

        private ViewHolder(View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.text_title);
            textDate = itemView.findViewById(R.id.text_date);
            textSite = itemView.findViewById(R.id.text_site);
            imageThumbnail = itemView.findViewById(R.id.image_thumbnail);
        }
    }

    public interface OnRecyclerListener {
        void onRecyclerClicked(View v, int position);
    }
}