package com.reedex.convenience;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

public class PagerAdapter extends FragmentPagerAdapter {
    private BaseFragment mCurrentFragment;
    PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: return AllFragment.newInstance();
            default: return SevenFragment.newInstance();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position) {
            case 0: title = "新着";
                break;
            case 1: title = "セブン";
                break;
        }
        return title;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (mCurrentFragment != object) {
            mCurrentFragment = (BaseFragment) object;
        }
        super.setPrimaryItem(container, position, object);
    }

    BaseFragment getCurrentFragment() {
        return mCurrentFragment;
    }
}
