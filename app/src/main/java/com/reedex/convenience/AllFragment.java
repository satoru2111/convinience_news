package com.reedex.convenience;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

public class AllFragment extends BaseFragment implements RecyclerAdapter.OnRecyclerListener {

    public AllFragment() {
        // Required empty public constructor
    }

    public static AllFragment newInstance() {
        return new AllFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void updateNews() {
        mSwipeRefresh.setRefreshing(true);
        resetFlag();
        requestNewsSokuhou();
    }
}
