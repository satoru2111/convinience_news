package com.reedex.convenience;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import hotchemi.android.rate.AppRate;
import hotchemi.android.rate.OnClickButtonListener;
import io.fabric.sdk.android.Fabric;
import io.realm.Realm;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    protected enum SITE {
        IT_SOKUHO("IT速報 仮想通貨"),
        COIN_SOKUHO("仮想通貨まとめNews"),
        BITCOIN_LABO("ビットコインラボ"),
        COIN_CHOICE("CoinChoice"),
        COIN_POST("CoinPost");
        String name;
        SITE(final String name) {
            this.name = name;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle(getString(R.string.app_name));

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        Realm.init(this);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, 0, 0);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final ViewPager viewPager = findViewById(R.id.viewpager);
        final PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        final TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {

            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {

                TextView tabTextView = new TextView(this);
                tab.setCustomView(tabTextView);
                tabTextView.setTextSize(getResources().getDimensionPixelSize(R.dimen.tab_unselected));
                tabTextView.setTextColor(getResources().getColor(R.color.white_focus));
                tabTextView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
                tabTextView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                tabTextView.setText(tab.getText());

                if (i == 0) {
                    tabTextView.setTypeface(null, Typeface.BOLD);
                    tabTextView.setTextSize(getResources().getDimensionPixelSize(R.dimen.tab_selected));
                    tabTextView.setTextColor(getResources().getColor(R.color.gray));
                }

            }

        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

                TextView text = (TextView) tab.getCustomView();
                text.setTextColor(getResources().getColor(R.color.gray));
                text.setTypeface(null, Typeface.BOLD);
                if (text.getText().toString().length() > 2) {
                    text.setTextSize(getResources().getDimensionPixelSize(R.dimen.tab_unselected));
                } else {
                    text.setTextSize(getResources().getDimensionPixelSize(R.dimen.tab_selected));
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                TextView text = (TextView) tab.getCustomView();
                text.setTextSize(getResources().getDimensionPixelSize(R.dimen.tab_unselected));
                text.setTextColor(getResources().getColor(R.color.white_focus));
                text.setTypeface(null, Typeface.NORMAL);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                TextView text = (TextView) tab.getCustomView();
                text.setTextSize(getResources().getDimensionPixelSize(R.dimen.tab_unselected));
                text.setTextColor(getResources().getColor(R.color.white_focus));
                text.setTypeface(null, Typeface.NORMAL);
            }
        });

        Toolbar mToolbar = findViewById(R.id.toolbar);
        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                BaseFragment fragment = adapter.getCurrentFragment();
                fragment.updateNews();
                return false;
            }
        });

        AppRate.setInstallDays(0).setLaunchTimes(3) // default 10
                .setShowNeutralButton(false)
                .setOnClickButtonListener(new OnClickButtonListener() { // callback listener.
                    @Override
                    public void onClickButton(int which) {
                        if (which == 1) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.ai.cryptocurrency"));
                            startActivity(intent);
                        }
                    }
                }).monitor(MainActivity.this);
        AppRate.showRateDialogIfMeetsConditions(MainActivity.this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.bigtoto) {
            try {
                PackageManager pm = getPackageManager();
                Intent intent = pm.getLaunchIntentForPackage("com.ai.big_toto");
                startActivity(intent);
            } catch (Exception e) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.ai.big_toto"));
                startActivity(intent);
            }
        } else if (id == R.id.recomend) {
            try {
                PackageManager pm = getPackageManager();
                Intent intent = pm.getLaunchIntentForPackage("com.ai.tousenkigan");
                startActivity(intent);
            } catch (Exception e) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.ai.tousenkigan"));
                startActivity(intent);
            }
        } else if (id == R.id.nav_share) {
            Intent intent = new Intent( Intent.ACTION_SEND )
                    .setType( "text/plain" )
                    .putExtra( Intent.EXTRA_TEXT, "仮想通貨速報　market://details?id=com.ai.cryptocurrency" );
            startActivity(intent);
        } else if (id == R.id.nav_send) {
            SendMailDialogFragment sendMailDialogFragment = new SendMailDialogFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(sendMailDialogFragment, "sendmail");
            ft.commitAllowingStateLoss();
        } else if (id == R.id.chart) {
            Intent intent = new Intent(this, WebViewActivity.class);
            intent.putExtra("url", "https://cc.minkabu.jp/chart");
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static class SendMailDialogFragment extends DialogFragment {

        @NonNull
        @Override
        public Dialog onCreateDialog(@NonNull Bundle savedInstanceState) {
            Dialog dialog;
            //showDialogを呼ぶときに１度だけ呼ばれる
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            dialogBuilder.setTitle("お問い合わせ");
            dialogBuilder.setMessage("メーラを起動します。" +
                    "\nあて先、件名は変更せず、本文にお問い合わせ内容を打ち込んでから送信してください。");
            dialogBuilder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Intentインスタンスを生成
                            Intent intent = new Intent();

                            // アクションを指定(ACTION_SENDTOではないところがミソ)
                            intent.setAction(Intent.ACTION_SENDTO);

                            // 宛先を指定
                            intent.setData(Uri.parse("mailto:satoru2111@gmail.com"));
                            // 件名を指定
                            intent.putExtra(Intent.EXTRA_SUBJECT, "仮想通貨速報に関するお問い合わせ");
                            // 本文を指定
                            intent.putExtra(Intent.EXTRA_TEXT, "お問い合わせ内容\n");

                            // Intentを発行
                            startActivity(intent);
                        }
                    });
            dialogBuilder.setNegativeButton("キャンセル",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
            dialog = dialogBuilder.create();
            return dialog;
        }
    }
}
