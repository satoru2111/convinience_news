package com.reedex.convenience;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;

public abstract class BaseFragment extends Fragment implements RecyclerAdapter.OnRecyclerListener {

    protected RecyclerView mRecyclerView;
    protected RecyclerAdapter mAdapter;
    protected List<Item> mItemList = new ArrayList<>();
    protected View mView;
    protected SwipeRefreshLayout mSwipeRefresh;
    protected SimpleDateFormat mFormatBase;
    protected SimpleDateFormat mFromatMMDD;

    private boolean mIsFinishSokuho = false;

    public BaseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFormatBase = new SimpleDateFormat("M.dd HH:mm", Locale.getDefault());
        mFromatMMDD = new SimpleDateFormat("MM/dd HH:mm");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_all, container, false);

            mSwipeRefresh = mView.findViewById(R.id.swipe_refresh);
            mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    updateNews();
                }
            });
            mSwipeRefresh.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefresh.setRefreshing(true);
                }
            });

            MobileAds.initialize(getContext(), getString(R.string.banner_ad_app_id));
            AdView mAdView = mView.findViewById(R.id.ad_view);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);

            mRecyclerView = mView.findViewById(R.id.recycler_view);
            mRecyclerView.addItemDecoration(new DividerItem(getContext()));
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            mAdapter = new RecyclerAdapter(getContext(), mItemList, BaseFragment.this);
            mRecyclerView.setAdapter(mAdapter);

            updateNews();
        }
        return mView;
    }

    protected void resetFlag() {
        if (this instanceof AllFragment) {
            mIsFinishSokuho = false;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onRecyclerClicked(View v, int position) {
        if (mItemList != null && mItemList.size() > position) {
            Intent intent = new Intent(getContext(), WebViewActivity.class);
            intent.putExtra("url", mItemList.get(position).link);
            startActivity(intent);
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            ReadHistory readHistory = realm.createObject(ReadHistory.class);
            readHistory.url = mItemList.get(position).link;
            realm.commitTransaction();
            realm.close();
            mItemList.get(position).isRead = true;
            mAdapter.notifyItemChanged(position);
        }
    }

    public abstract void updateNews();

    private void sortList() {
        Collections.sort(mItemList, new Comparator<Item>() {
            @Override
            public int compare(Item o1, Item o2) {
                try {
                    return mFormatBase.parse(o2.date).compareTo(mFormatBase.parse(o1.date));
                } catch (ParseException e) {
                    return 0;
                }
            }
        });
    }

    private void getReadHistory(Item item) {
        Realm realm = Realm.getDefaultInstance();
        ReadHistory readHistory = realm.where(ReadHistory.class).equalTo("url", item.link).findFirst();
        if (readHistory != null) {
            item.isRead = true;
        }
    }

    private void dismissDialog() {
        if (this instanceof AllFragment) {
            if (mIsFinishSokuho) {
                if (mSwipeRefresh != null && mSwipeRefresh.isRefreshing()) {
                    mSwipeRefresh.setRefreshing(false);
                }
            }
        } else if (this instanceof SevenFragment) {
            if (mIsFinishSokuho) {
                if (mSwipeRefresh != null && mSwipeRefresh.isRefreshing()) {
                    mSwipeRefresh.setRefreshing(false);
                }
            }
        }
    }

    protected void requestNewsSokuhou() {
        StringRequest request = new StringRequest(Request.Method.GET, "https://kasoutuka-navi.com/newssokuhou/",
                new Response.Listener<String>() {
                    // レスポンス受信のリスナー
                    @Override
                    public void onResponse(String response) {
                        updateNewsSokuhou(response);
                        mIsFinishSokuho = true;
                        sortList();
                        mAdapter.notifyDataSetChanged();
                        dismissDialog();
                    }
                },
                new Response.ErrorListener() {
                    // リクエストエラーのリスナー
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        AppNetworkSingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private String changeDateBaseMMDD(String dataString) {
        Date date = null;
        try {
            date = mFromatMMDD.parse(dataString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return mFormatBase.format(date);
    }

    private void updateNewsSokuhou(String response) {
        boolean isSkipped = false;
        final long start = System.currentTimeMillis();
        try {
            Document document = Jsoup.parse(response);
            List<Element> elementNewsList = document.getElementsByClass("dp_sc_table_body").get(0).getElementsByTag("tr");
            for (Element elementList : elementNewsList) {
                try {
                    String url = elementList.getElementsByAttribute("href").get(1).attr("href");
                    if (isExistUrl(url)) {
                        if (isSkipped) {
                            mIsFinishSokuho = true;
                            dismissDialog();
                            break;
                        }
                        isSkipped = true;
                        continue;
                    }
                    isSkipped = false;

                    Item item = new Item();
                    item.link = url;
                    item.title = elementList.getElementsByAttribute("href").get(1).html();
                    item.date = changeDateBaseMMDD(elementList.getElementsByTag("td").get(0).html());
                    item.site = elementList.getElementsByAttribute("href").get(0).html();
                    getReadHistory(item);
                    mItemList.add(item);
                } catch (Exception e) {
                    Log.e("error inside", "error = " + e.getMessage());
                }
            }
        } catch (Exception e) {
            Log.e("error", "error = " + e.getMessage());
        }
        long end = System.currentTimeMillis();
        Log.e("time", "updateNewsSokuhou" + (end - start) + "ms");
    }

    private boolean isExistUrl(String url) {
        for (Item item : mItemList) {
            if (item.link.equals(url)) {
                return true;
            }
        }

        return false;
    }
}
